import BlogRoute from '.routes/blog.route';
import request from 'supertest';
import App from './app';

const samples = [1, 2, 3].map(id => ({ id: id, title: `title:${id}`, content: `content:${id}` }));

afterAll(async () => {
  await new Promise<void>(resolve => setTimeout(() => resolve(), 500));
});

describe('Testing Blogs', () => {
  describe('[GET] /blog', () => {
    it('response findAll blogs', async () => {
      const blogsRoute = new BlogRoute();
      const blogs = blogsRoute.blogController.blogService.blogs;

      blogs.findMany = jest.fn().mockReturnValue(samples);

      const app = new App([blogsRoute]);
      return request(app.getServer()).get(`${blogsRoute.path}`).expect(200);
    });
  });

  describe('[GET] /blogs/:id', () => {
    it('response findOne blog', async () => {
      const blogsRoute = new BlogRoute();
      const blogs = blogsRoute.blogController.blogService.blogs;

      blogs.findUnique = jest.fn().mockReturnValue(samples[0]);

      const app = new App([blogsRoute]);
      return request(app.getServer()).get(`${blogsRoute.path}/${samples[0].id}`).expect(200);
    });
  });

  describe('[POST] /blogs', () => {
    it('response Create blog', async () => {
      const blogsRoute = new BlogRoute();
      const blogs = blogsRoute.blogController.blogService.blogs;

      blogs.findUnique = jest.fn().mockReturnValue(null);
      blogs.create = jest.fn().mockReturnValue(samples[0]);

      const app = new App([blogsRoute]);
      return request(app.getServer()).post(`${blogsRoute.path}`).send(samples[0]).expect(201);
    });
  });

  describe('[PUT] /blogs/:id', () => {
    it('response Update blog', async () => {
      const updateData = {
        id: samples[0].id,
        title: samples[0].title,
        content: 'content changed',
      };

      const blogsRoute = new BlogRoute();
      const blogs = blogsRoute.blogController.blogService.blogs;

      blogs.findUnique = jest.fn().mockReturnValue(samples[0]);
      blogs.update = jest.fn().mockReturnValue({
        id: updateData.id,
        title: updateData.title,
        content: updateData.content,
      });

      const app = new App([blogsRoute]);
      return request(app.getServer()).put(`${blogsRoute.path}/${updateData.id}`).send(updateData).expect(200);
    });
  });

  describe('[DELETE] /blogs/:id', () => {
    it('response Delete blog', async () => {
      const blogsRoute = new BlogRoute();
      const blogs = blogsRoute.blogController.blogService.blogs;

      blogs.delete = jest.fn().mockReturnValue(samples[0]);

      const app = new App([blogsRoute]);
      return request(app.getServer()).delete(`${blogsRoute.path}/${samples[0].id}`).expect(200);
    });
  });
});
