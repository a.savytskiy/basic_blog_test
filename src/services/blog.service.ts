import { Blog, PrismaClient } from '.prisma/client';
import { CreateBlogDto } from '../dtos/blog.dto';

export class BlogService {
  public blogs = new PrismaClient().blog;

  public async findAllBlog(): Promise<Blog[]> {
    const allBlogs: Blog[] = await this.blogs.findMany();
    return allBlogs;
  }

  public async findBlogById(id: number): Promise<Blog> {
    const findBlog: Blog = await this.blogs.findUnique({ where: { id: id } });

    return findBlog;
  }

  public async createBlog(blogData: CreateBlogDto): Promise<Blog> {
    const createBlogData: Blog = await this.blogs.create({ data: { ...blogData } });
    return createBlogData;
  }

  public async updateBlog(id: number, blogData: CreateBlogDto): Promise<Blog> {
    const updateBlogData = await this.blogs.update({ where: { id: id }, data: { ...blogData } });
    return updateBlogData;
  }

  public async deleteBlog(id: number): Promise<Blog> {
    const deleteBlogData = await this.blogs.delete({ where: { id: id } });
    return deleteBlogData;
  }
}
