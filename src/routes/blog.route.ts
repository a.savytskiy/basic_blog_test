import { Router } from 'express';
import { BlogController } from '../controllers/blog.controller';

export interface Routes {
  path?: string;
  router: Router;
}

export class BlogRoute implements Routes {
  public path = '/blog';
  public router = Router();
  public blogController = new BlogController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.blogController.getBlogs);
    this.router.get(`${this.path}/:id(\\d+)`, this.blogController.getBlogById);
    this.router.post(`${this.path}`, this.blogController.createBlog);
    this.router.put(`${this.path}/:id(\\d+)`, this.blogController.updateBlog);
    this.router.delete(`${this.path}/:id(\\d+)`, this.blogController.deleteBlog);
  }
}
