import { Blog } from '.prisma/client';
import { NextFunction, Request, Response } from 'express';
import { CreateBlogDto } from '../dtos/blog.dto';
import { BlogService } from '../services/blog.service';

export class BlogController {
  public blogService = new BlogService();

  public getBlogs = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllBlogsData: Blog[] = await this.blogService.findAllBlog();

      res.status(200).json({ data: findAllBlogsData });
    } catch (error) {
      next(error);
    }
  };

  public getBlogById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const id = Number(req.params.id);
      const findOneBlogData: Blog = await this.blogService.findBlogById(id);

      res.status(200).json({ data: findOneBlogData });
    } catch (error) {
      next(error);
    }
  };

  public createBlog = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const blogData: CreateBlogDto = req.body;
      const createBlogData: Blog = await this.blogService.createBlog(blogData);

      res.status(201).json({ data: createBlogData });
    } catch (error) {
      next(error);
    }
  };

  public updateBlog = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const id = Number(req.params.id);
      const blogData: CreateBlogDto = req.body;
      const updateBlogData: Blog = await this.blogService.updateBlog(id, blogData);

      res.status(200).json({ data: updateBlogData });
    } catch (error) {
      next(error);
    }
  };

  public deleteBlog = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const id = Number(req.params.id);
      const deleteBlogData: Blog = await this.blogService.deleteBlog(id);
      res.status(200).json({ data: deleteBlogData });
    } catch (error) {
      next(error);
    }
  };
}
