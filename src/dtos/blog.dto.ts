import { IsDateString, IsEmail, IsString } from 'class-validator';

export class CreateBlogDto {
  @IsEmail()
  public title: string;

  @IsString()
  public content: string;

  @IsDateString()
  public publishedAt: string;
}
